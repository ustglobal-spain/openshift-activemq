# README #

This is sample code for obtaining metrics from an active mq container in OpenShift using Prometheus.

This sample will use Grafana for showing the metrics.

## Dockerfile

The image files in docker folder has a modified version of the active mq image, adding the prometheus agent for JMX.

It is possible to build locally with the commands:

```
cd docker\rhel-amq62-openshift
make build-docker
```
And for testing the images with:
```
make run-docker
```
## Prometheus agent config

The file prometheus-agent-config.yaml will configure the outputs for the exporter. If we want to see al the values to obtain we can se the exporter with the values:

```
---
lowercaseOutputLabelNames: true
lowercaseOutputName: true
rules:
- pattern: ".*"

```

That will extract all the JMX parameters. We can see them inside the container execution:

```
docker exec jboss-activemq curl http://localhost:9090
```

As a sample (in /samples/sample-jmx-raw.log) we can see all parameters from JMX, for example:

```
# HELP org_apache_activemq_broker_forwardcount Number of messages that have been forwarded (to a networked broker) from the destination. (org.apache.activemq<type=Broker, brokerName=2ded91d4d55d, destinationType=Topic, destinationName=ActiveMQ.Advisory.MasterBroker><>ForwardCount)
# TYPE org_apache_activemq_broker_forwardcount untyped
org_apache_activemq_broker_forwardcount{brokername="2ded91d4d55d",destinationtype="Topic",destinationname="ActiveMQ.Advisory.MasterBroker",} 0.0
# HELP org_apache_activemq_broker_memoryusageportion Portion of memory from the broker memory limit for this destination (org.apache.activemq<type=Broker, brokerName=2ded91d4d55d, destinationType=Topic, destinationName=ActiveMQ.Advisory.MasterBroker><>MemoryUsagePortion)
# TYPE org_apache_activemq_broker_memoryusageportion untyped
org_apache_activemq_broker_memoryusageportion{brokername="2ded91d4d55d",destinationtype="Topic",destinationname="ActiveMQ.Advisory.MasterBroker",} 1.0
# HELP org_apache_activemq_broker_consumercount Number of consumers subscribed to this destination. (org.apache.activemq<type=Broker, brokerName=2ded91d4d55d, destinationType=Topic, destinationName=ActiveMQ.Advisory.MasterBroker><>ConsumerCount)
# TYPE org_apache_activemq_broker_consumercount untyped
org_apache_activemq_broker_consumercount{brokername="2ded91d4d55d",destinationtype="Topic",destinationname="ActiveMQ.Advisory.MasterBroker",} 0.0
# HELP java_lang_operatingsystem_committedvirtualmemorysize CommittedVirtualMemorySize (java.lang<type=OperatingSystem><>CommittedVirtualMemorySize)
# TYPE java_lang_operatingsystem_committedvirtualmemorysize untyped
java_lang_operatingsystem_committedvirtualmemorysize 2.275176448E9
# HELP java_lang_memory_objectpendingfinalizationcount ObjectPendingFinalizationCount (java.lang<type=Memory><>ObjectPendingFinalizationCount)
# TYPE java_lang_memory_objectpendingfinalizationcount untyped
java_lang_memory_objectpendingfinalizationcount 0.0
# HELP org_apache_activemq_broker_jobschedulerstorepercentusage Percent of job store limit used. (org.apache.activemq<type=Broker, brokerName=2ded91d4d55d><>JobSchedulerStorePercentUsage)
# TYPE org_apache_activemq_broker_jobschedulerstorepercentusage untyped
org_apache_activemq_broker_jobschedulerstorepercentusage{brokername="2ded91d4d55d",} 0.0
```

This will show all the data without format, it will be a good practice to reduce the data, and filter. We propose the sample template as default:

```
---
lowercaseOutputLabelNames: true
lowercaseOutputName: true
rules:
- pattern: 'org.apache.activemq<type=Broker, brokerName=(.*)><>(TotalConnectionsCount)'
  name: org_apache_activemq_$2
  labels:
    brokerName: "$1"  
  type: COUNTER
  
- pattern: 'org.apache.activemq<type=Broker, brokerName=(.*)><>(CurrentConnectionsCount|TotalConnectionsCount|TotalEnqueueCount|TotalDequeueCount|TotalConsumerCount|TotalProducerCount|TotalMessageCount|AverageMessageSize)'
  name: org_apache_activemq_$2
  labels:
    brokerName: "$1"  
  type: GAUGE
- pattern: 'org.apache.activemq<type=Broker, brokerName=(.*),><>(MemoryPercentUsage)'
  name: org_apache_activemq_$2
  labels:
    brokerName: "$1"  
  type: GAUGE
- pattern: 'org.apache.activemq<type=Broker, brokerName=(.*), destinationType=(.*), destinationName=(.*)><>(QueueSize)'
  name: org_apache_activemq_$4
  labels:
    brokerName: "$1"
    destinationType: "$2"
    destinationName: "$3"
  type: GAUGE
```

In the [prometheus JMX exporter](https://github.com/prometheus/jmx_exporter/tree/master/example_configs) we can find more samples.


Once we have configured the prometheus server for obtaining the metrics, it is possible to get them in grafana

In samples/grafana-activemq-producer-consumer it is included an example template for obtaining a graph from a workspace.

We can create our own graphs usint the prometheus language. For example for obtaining all the producers values it is possible with the query:

```
sum(org_apache_activemq_totalproducercount{kubernetes_namespace="yournamespace"})
```
